#!/bin/bash

set -e
set -x

script_dir=$(dirname "$0")

if [ -z "$1" ]
then
  src_dir=$script_dir/../src
else
  src_dir=${1}
fi

if [ -z "$2" ]
then
  dist_dir=${script_dir}/../public
else
  dist_dir=${2}
fi

mkdir -p $dist_dir

rsync -aH $src_dir/ $dist_dir

# Aquila manual
# apply supplement SB-AT01-029 (B.01) to FM_AT01_1010_100E_B04 (B.04)
if [ -f "${dist_dir}/FM_AT01_1010_100E_B04.pdf" ] && [ -f "${dist_dir}SB_AT01_029_02-AFM_page_1-8_2-4_5.pdf" ]; then
  pdftk A=${dist_dir}/FM_AT01_1010_100E_B04.pdf B=publicSB_AT01_029_02-AFM_page_1-8_2-4_5.pdf cat A1-16 B1 A18-24 B2 B3 A27-end output ${dist_dir}/aquila-a210/FM_AT01_1010_100E_B04____SB_AT01_029_02-AFM_page_1-8_2-4_5.pdf
fi

# Split VH-BNE manual
vh_bne_manual_path="vh-bne/aquila-a210_vh-bne"
vh_bne_manual="${dist_dir}/${vh_bne_manual_path}"
vh_bne_manual_pdf="${vh_bne_manual}.pdf"
if [ -f "${vh_bne_manual_pdf}" ]; then
  pdftk ${vh_bne_manual_pdf} cat 1 output ${dist_dir}/${vh_bne_manual_path}_cover.pdf
  pdftk ${vh_bne_manual_pdf} cat 2-9 output ${dist_dir}/${vh_bne_manual_path}_introduction.pdf
  pdftk ${vh_bne_manual_pdf} cat 10-21 output ${dist_dir}/${vh_bne_manual_path}_section1_general.pdf
  pdftk ${vh_bne_manual_pdf} cat 22-46 output ${dist_dir}/${vh_bne_manual_path}_section2_limitations.pdf
  pdftk ${vh_bne_manual_pdf} cat 47-62 output ${dist_dir}/${vh_bne_manual_path}_section3_emergency_procedures.pdf
  pdftk ${vh_bne_manual_pdf} cat 63-82 output ${dist_dir}/${vh_bne_manual_path}_section4_normal_procedures.pdf
  pdftk ${vh_bne_manual_pdf} cat 83-117 output ${dist_dir}/${vh_bne_manual_path}_section5_performance.pdf
  pdftk ${vh_bne_manual_pdf} cat 118-132 output ${dist_dir}/${vh_bne_manual_path}_section6_weight_and_balance_equipment_list.pdf
  pdftk ${vh_bne_manual_pdf} cat 133-164 output ${dist_dir}/${vh_bne_manual_path}_section7_description_of_the_aircraft_and_its_systems.pdf
  pdftk ${vh_bne_manual_pdf} cat 165-172 output ${dist_dir}/${vh_bne_manual_path}_section8_handling_service_maintenance.pdf
  pdftk ${vh_bne_manual_pdf} cat 173-254 output ${dist_dir}/${vh_bne_manual_path}_section9_supplements.pdf
    pdftk ${vh_bne_manual_pdf} cat 178-183 output ${dist_dir}/${vh_bne_manual_path}_section9_supplements_ave4.pdf
    pdftk ${vh_bne_manual_pdf} cat 184-189 output ${dist_dir}/${vh_bne_manual_path}_section9_supplements_ave5.pdf
    pdftk ${vh_bne_manual_pdf} cat 190-193 output ${dist_dir}/${vh_bne_manual_path}_section9_supplements_ave10.pdf
    pdftk ${vh_bne_manual_pdf} cat 194-203 output ${dist_dir}/${vh_bne_manual_path}_section9_supplements_ave11.pdf
    pdftk ${vh_bne_manual_pdf} cat 204-214 output ${dist_dir}/${vh_bne_manual_path}_section9_supplements_ave12.pdf
    pdftk ${vh_bne_manual_pdf} cat 215-226 output ${dist_dir}/${vh_bne_manual_path}_section9_supplements_ave16.pdf
    pdftk ${vh_bne_manual_pdf} cat 227-244 output ${dist_dir}/${vh_bne_manual_path}_section9_supplements_ave23.pdf
    pdftk ${vh_bne_manual_pdf} cat 245-254 output ${dist_dir}/${vh_bne_manual_path}_section9_supplements_ave26.pdf
fi

# Split 24-4844 Manual
eurofox_24_4844_manual_path="24-4844/eurofox-3k_24-4844"
eurofox_24_4844_manual="${dist_dir}/${eurofox_24_4844_manual_path}"
eurofox_24_4844_manual_pdf="${eurofox_24_4844_manual}.pdf"
if [ -f "${eurofox_24_4844_manual_pdf}" ]; then
  pdftk ${eurofox_24_4844_manual_pdf} cat 1 output ${dist_dir}/${eurofox_24_4844_manual_path}_cover.pdf
  pdftk ${eurofox_24_4844_manual_pdf} cat 2-4 output ${dist_dir}/${eurofox_24_4844_manual_path}_introduction.pdf
  pdftk ${eurofox_24_4844_manual_pdf} cat 5-8 output ${dist_dir}/${eurofox_24_4844_manual_path}_table_of_contents.pdf
  pdftk ${eurofox_24_4844_manual_pdf} cat 9 output ${dist_dir}/${eurofox_24_4844_manual_path}_general_information.pdf
  pdftk ${eurofox_24_4844_manual_pdf} cat 10-21 output ${dist_dir}/${eurofox_24_4844_manual_path}_airplane_and_systems_description.pdf
  pdftk ${eurofox_24_4844_manual_pdf} cat 22-25 output ${dist_dir}/${eurofox_24_4844_manual_path}_operating_limitations.pdf
  pdftk ${eurofox_24_4844_manual_pdf} cat 26-29 output ${dist_dir}/${eurofox_24_4844_manual_path}_weight_and_balance_information.pdf
  pdftk ${eurofox_24_4844_manual_pdf} cat 30-32 output ${dist_dir}/${eurofox_24_4844_manual_path}_performance.pdf
  pdftk ${eurofox_24_4844_manual_pdf} cat 33-41 output ${dist_dir}/${eurofox_24_4844_manual_path}_emergency_procedures.pdf
  pdftk ${eurofox_24_4844_manual_pdf} cat 42-57 output ${dist_dir}/${eurofox_24_4844_manual_path}_normal_procedures.pdf
  pdftk ${eurofox_24_4844_manual_pdf} cat 58-60 output ${dist_dir}/${eurofox_24_4844_manual_path}_aircraft_ground_handling_and_servicing.pdf
  pdftk ${eurofox_24_4844_manual_pdf} cat 61-63 output ${dist_dir}/${eurofox_24_4844_manual_path}_required_placards_and_markings.pdf
  pdftk ${eurofox_24_4844_manual_pdf} cat 64 output ${dist_dir}/${eurofox_24_4844_manual_path}_supplementary_information.pdf
fi

# Split 24-5350 Manual
eurofox_24_5350_manual_path="24-5350/eurofox-3k_24-5350"
eurofox_24_5350_manual="${dist_dir}/${eurofox_24_5350_manual_path}"
eurofox_24_5350_manual_pdf="${eurofox_24_5350_manual}.pdf"
if [ -f "${eurofox_24_5350_manual_pdf}" ]; then
  pdftk ${eurofox_24_5350_manual_pdf} cat 1 output ${dist_dir}/${eurofox_24_5350_manual_path}_cover.pdf
  pdftk ${eurofox_24_5350_manual_pdf} cat 2-3 output ${dist_dir}/${eurofox_24_5350_manual_path}_introduction.pdf
  pdftk ${eurofox_24_5350_manual_pdf} cat 4-7 output ${dist_dir}/${eurofox_24_5350_manual_path}_table_of_contents.pdf
  pdftk ${eurofox_24_5350_manual_pdf} cat 8 output ${dist_dir}/${eurofox_24_5350_manual_path}_general_information.pdf
  pdftk ${eurofox_24_5350_manual_pdf} cat 9-20 output ${dist_dir}/${eurofox_24_5350_manual_path}_airplane_and_systems_description.pdf
  pdftk ${eurofox_24_5350_manual_pdf} cat 21-24 output ${dist_dir}/${eurofox_24_5350_manual_path}_operating_limitations.pdf
  pdftk ${eurofox_24_5350_manual_pdf} cat 25-29 output ${dist_dir}/${eurofox_24_5350_manual_path}_weight_and_balance_information.pdf
  pdftk ${eurofox_24_5350_manual_pdf} cat 30-32 output ${dist_dir}/${eurofox_24_5350_manual_path}_performance.pdf
  pdftk ${eurofox_24_5350_manual_pdf} cat 33-41 output ${dist_dir}/${eurofox_24_5350_manual_path}_emergency_procedures.pdf
  pdftk ${eurofox_24_5350_manual_pdf} cat 42-57 output ${dist_dir}/${eurofox_24_5350_manual_path}_normal_procedures.pdf
  pdftk ${eurofox_24_5350_manual_pdf} cat 58-60 output ${dist_dir}/${eurofox_24_5350_manual_path}_aircraft_ground_handling_and_servicing.pdf
  pdftk ${eurofox_24_5350_manual_pdf} cat 61-63 output ${dist_dir}/${eurofox_24_5350_manual_path}_required_placards_and_markings.pdf
  pdftk ${eurofox_24_5350_manual_pdf} cat 64 output ${dist_dir}/${eurofox_24_5350_manual_path}_supplementary_information.pdf
fi

# Split 24-8881 Manual
eurofox_24_8881_manual_path="24-8881/eurofox-3k_24-8881"
eurofox_24_8881_manual="${dist_dir}/${eurofox_24_8881_manual_path}"
eurofox_24_8881_manual_pdf="${eurofox_24_8881_manual}.pdf"
if [ -f "${eurofox_24_8881_manual_pdf}" ]; then
  pdftk ${eurofox_24_8881_manual_pdf} cat 1 output ${dist_dir}/${eurofox_24_8881_manual_path}_cover.pdf
  pdftk ${eurofox_24_8881_manual_pdf} cat 2-10 output ${dist_dir}/${eurofox_24_8881_manual_path}_introduction.pdf
  pdftk ${eurofox_24_8881_manual_pdf} cat 11-14 output ${dist_dir}/${eurofox_24_8881_manual_path}_table_of_contents.pdf
  pdftk ${eurofox_24_8881_manual_pdf} cat 15-16 output ${dist_dir}/${eurofox_24_8881_manual_path}_general_information.pdf
  pdftk ${eurofox_24_8881_manual_pdf} cat 17-28 output ${dist_dir}/${eurofox_24_8881_manual_path}_airplane_and_systems_description.pdf
  pdftk ${eurofox_24_8881_manual_pdf} cat 29-32 output ${dist_dir}/${eurofox_24_8881_manual_path}_operating_limitations.pdf
  pdftk ${eurofox_24_8881_manual_pdf} cat 33-39 output ${dist_dir}/${eurofox_24_8881_manual_path}_weight_and_balance_information.pdf
  pdftk ${eurofox_24_8881_manual_pdf} cat 40-42 output ${dist_dir}/${eurofox_24_8881_manual_path}_performance.pdf
  pdftk ${eurofox_24_8881_manual_pdf} cat 43-51 output ${dist_dir}/${eurofox_24_8881_manual_path}_emergency_procedures.pdf
  pdftk ${eurofox_24_8881_manual_pdf} cat 52-67 output ${dist_dir}/${eurofox_24_8881_manual_path}_normal_procedures.pdf
  pdftk ${eurofox_24_8881_manual_pdf} cat 68-69 output ${dist_dir}/${eurofox_24_8881_manual_path}_aircraft_ground_handling_and_servicing.pdf
  pdftk ${eurofox_24_8881_manual_pdf} cat 70-72 output ${dist_dir}/${eurofox_24_8881_manual_path}_required_placards_and_markings.pdf
  pdftk ${eurofox_24_8881_manual_pdf} cat 73 output ${dist_dir}/${eurofox_24_8881_manual_path}_supplementary_information.pdf
fi

# Split Eurofox 3K Factory Manual
eurofox_3k_factory_manual_path="a240-poh"
eurofox_3k_factory_manual="${dist_dir}/${eurofox_3k_factory_manual_path}"
eurofox_3k_factory_manual_pdf="${eurofox_3k_factory_manual}.pdf"
if [ -f "${eurofox_3k_factory_manual_pdf}" ]; then
  pdftk ${eurofox_3k_factory_manual_pdf} cat 1 output ${dist_dir}/${eurofox_3k_factory_manual_path}_cover.pdf
  pdftk ${eurofox_3k_factory_manual_pdf} cat 4-6 output ${dist_dir}/${eurofox_3k_factory_manual_path}_table_of_contents.pdf
  pdftk ${eurofox_3k_factory_manual_pdf} cat 7-7 output ${dist_dir}/${eurofox_3k_factory_manual_path}_general_information.pdf
  pdftk ${eurofox_3k_factory_manual_pdf} cat 8-17 output ${dist_dir}/${eurofox_3k_factory_manual_path}_airplane_and_systems_description.pdf
  pdftk ${eurofox_3k_factory_manual_pdf} cat 18-20 output ${dist_dir}/${eurofox_3k_factory_manual_path}_operating_limitations.pdf
  pdftk ${eurofox_3k_factory_manual_pdf} cat 21-23 output ${dist_dir}/${eurofox_3k_factory_manual_path}_weight_and_balance_information.pdf
  pdftk ${eurofox_3k_factory_manual_pdf} cat 24-25 output ${dist_dir}/${eurofox_3k_factory_manual_path}_performance.pdf
  pdftk ${eurofox_3k_factory_manual_pdf} cat 26-34 output ${dist_dir}/${eurofox_3k_factory_manual_path}_normal_procedures.pdf
  pdftk ${eurofox_3k_factory_manual_pdf} cat 35-40 output ${dist_dir}/${eurofox_3k_factory_manual_path}_emergency_procedures.pdf
  pdftk ${eurofox_3k_factory_manual_pdf} cat 41-44 output ${dist_dir}/${eurofox_3k_factory_manual_path}_aircraft_ground_handling_and_servicing.pdf
  pdftk ${eurofox_3k_factory_manual_pdf} cat 45-45 output ${dist_dir}/${eurofox_3k_factory_manual_path}_supplementary_information.pdf
  pdftk ${eurofox_3k_factory_manual_pdf} cat 46-50 output ${dist_dir}/${eurofox_3k_factory_manual_path}_flight_training_supplement.pdf
fi

# Split Eurofox 3K Maintenance Manual
eurofox_3k_maintenance_manual_path="maintenance-manual"
eurofox_3k_maintenance_manual="${dist_dir}/${eurofox_3k_maintenance_manual_path}"
eurofox_3k_maintenance_manual_pdf="${eurofox_3k_maintenance_manual}.pdf"
if [ -f "${eurofox_3k_maintenance_manual_pdf}" ]; then
  pdftk ${eurofox_3k_maintenance_manual_pdf} cat 1 output ${dist_dir}/${eurofox_3k_maintenance_manual_path}_cover.pdf
  pdftk ${eurofox_3k_maintenance_manual_pdf} cat 2 output ${dist_dir}/${eurofox_3k_maintenance_manual_path}_list_of_effective_pages.pdf
  pdftk ${eurofox_3k_maintenance_manual_pdf} cat 3 output ${dist_dir}/${eurofox_3k_maintenance_manual_path}_table_of_contents.pdf
  pdftk ${eurofox_3k_maintenance_manual_pdf} cat 4 output ${dist_dir}/${eurofox_3k_maintenance_manual_path}_foreword.pdf
  pdftk ${eurofox_3k_maintenance_manual_pdf} cat 5 output ${dist_dir}/${eurofox_3k_maintenance_manual_path}_common_conversions_and_abbreviations.pdf
  pdftk ${eurofox_3k_maintenance_manual_pdf} cat 6-13 output ${dist_dir}/${eurofox_3k_maintenance_manual_path}_section1_general_description.pdf
  pdftk ${eurofox_3k_maintenance_manual_pdf} cat 14-24 output ${dist_dir}/${eurofox_3k_maintenance_manual_path}_section2_ground_handling_servicing_lubrication_and_inspection.pdf
  pdftk ${eurofox_3k_maintenance_manual_pdf} cat 25-33 output ${dist_dir}/${eurofox_3k_maintenance_manual_path}_section3_structures_fuselage.pdf
  pdftk ${eurofox_3k_maintenance_manual_pdf} cat 34-41 output ${dist_dir}/${eurofox_3k_maintenance_manual_path}_section4_structures_wings_and_empennage.pdf
  pdftk ${eurofox_3k_maintenance_manual_pdf} cat 42-54 output ${dist_dir}/${eurofox_3k_maintenance_manual_path}_section5_structures_landing_gear_and_brakes_tricycle_gear.pdf
  pdftk ${eurofox_3k_maintenance_manual_pdf} cat 55-61 output ${dist_dir}/${eurofox_3k_maintenance_manual_path}_section6_structures_aileron_and_flap_control_system.pdf
  pdftk ${eurofox_3k_maintenance_manual_pdf} cat 62-65 output ${dist_dir}/${eurofox_3k_maintenance_manual_path}_section7_structures_elevator_control_system.pdf
  pdftk ${eurofox_3k_maintenance_manual_pdf} cat 66-69 output ${dist_dir}/${eurofox_3k_maintenance_manual_path}_section8_structures_elevator_trim_control_system.pdf
  pdftk ${eurofox_3k_maintenance_manual_pdf} cat 70-72 output ${dist_dir}/${eurofox_3k_maintenance_manual_path}_section9_structures_rudder_control_system.pdf
  pdftk ${eurofox_3k_maintenance_manual_pdf} cat 72-94 output ${dist_dir}/${eurofox_3k_maintenance_manual_path}_section10_engine.pdf
  pdftk ${eurofox_3k_maintenance_manual_pdf} cat 95-101 output ${dist_dir}/${eurofox_3k_maintenance_manual_path}_section11_fuel_system.pdf
  pdftk ${eurofox_3k_maintenance_manual_pdf} cat 102-103 output ${dist_dir}/${eurofox_3k_maintenance_manual_path}_section12_propeller.pdf
  pdftk ${eurofox_3k_maintenance_manual_pdf} cat 104-106 output ${dist_dir}/${eurofox_3k_maintenance_manual_path}_section13_utility_systems.pdf
  pdftk ${eurofox_3k_maintenance_manual_pdf} cat 107-114 output ${dist_dir}/${eurofox_3k_maintenance_manual_path}_section14_instruments_and_instrument_system.pdf
  pdftk ${eurofox_3k_maintenance_manual_pdf} cat 115-123 output ${dist_dir}/${eurofox_3k_maintenance_manual_path}_section15_electrical_systems.pdf
  pdftk ${eurofox_3k_maintenance_manual_pdf} cat 124 output ${dist_dir}/${eurofox_3k_maintenance_manual_path}_section16_structural_repair.pdf
  pdftk ${eurofox_3k_maintenance_manual_pdf} cat 125 output ${dist_dir}/${eurofox_3k_maintenance_manual_path}_section17_exterior_painting.pdf
  pdftk ${eurofox_3k_maintenance_manual_pdf} cat 126 output ${dist_dir}/${eurofox_3k_maintenance_manual_path}_section18_wiring_diagrams_general_wiring_scheme.pdf
  pdftk ${eurofox_3k_maintenance_manual_pdf} cat 127-128 output ${dist_dir}/${eurofox_3k_maintenance_manual_path}_section19_safety_directives_and_safety_monitoring_system.pdf
  pdftk ${eurofox_3k_maintenance_manual_pdf} cat 129-141 output ${dist_dir}/${eurofox_3k_maintenance_manual_path}_a220_taildragger_supplement.pdf
fi

# Split VH-RMV Manual
vh_rmv_manual_path="vh-rmv/vans-rv14_vh-rmv"
vh_rmv_manual="${dist_dir}/${vh_rmv_manual_path}"
vh_rmv_manual_pdf="${vh_rmv_manual}.pdf"
if [ -f "${vh_rmv_manual_pdf}" ]; then
  pdftk ${vh_rmv_manual_pdf} cat 1 output ${dist_dir}/${vh_rmv_manual_path}_cover.pdf
  pdftk ${vh_rmv_manual_pdf} cat 2-3 output ${dist_dir}/${vh_rmv_manual_path}_equipment_list.pdf
  pdftk ${vh_rmv_manual_pdf} cat 4-5 output ${dist_dir}/${vh_rmv_manual_path}_performance_specifications.pdf
  pdftk ${vh_rmv_manual_pdf} cat 7 output ${dist_dir}/${vh_rmv_manual_path}_aerobatic_information.pdf
  pdftk ${vh_rmv_manual_pdf} cat 8-9 output ${dist_dir}/${vh_rmv_manual_path}_preflight_inspection.pdf
  pdftk ${vh_rmv_manual_pdf} cat 10 output ${dist_dir}/${vh_rmv_manual_path}_starting_engine.pdf
  pdftk ${vh_rmv_manual_pdf} cat 11 output ${dist_dir}/${vh_rmv_manual_path}_before_takeoff.pdf
  pdftk ${vh_rmv_manual_pdf} cat 12 output ${dist_dir}/${vh_rmv_manual_path}_takeoff.pdf
  pdftk ${vh_rmv_manual_pdf} cat 13 output ${dist_dir}/${vh_rmv_manual_path}_landing.pdf
  pdftk ${vh_rmv_manual_pdf} cat 14 output ${dist_dir}/${vh_rmv_manual_path}_performance.pdf
  pdftk ${vh_rmv_manual_pdf} cat 15 output ${dist_dir}/${vh_rmv_manual_path}_engine_information.pdf
  pdftk ${vh_rmv_manual_pdf} cat 17-20 output ${dist_dir}/${vh_rmv_manual_path}_emergency_procedures.pdf
  pdftk ${vh_rmv_manual_pdf} cat 21-25 output ${dist_dir}/${vh_rmv_manual_path}_weight_and_balance.pdf
fi

# Split VH-EGY Manual
vh_egy_manual_path="vh-egy/cessna-152_vh-egy"
vh_egy_manual="${dist_dir}/${vh_egy_manual_path}"
vh_egy_manual_pdf="${vh_egy_manual}.pdf"
if [ -f "${vh_egy_manual_pdf}" ]; then
  pdftk ${vh_egy_manual_pdf} cat 1-2 output ${dist_dir}/${vh_egy_manual_path}_certificate_of_registration.pdf
  pdftk ${vh_egy_manual_pdf} cat 3 output ${dist_dir}/${vh_egy_manual_path}_cover.pdf
  pdftk ${vh_egy_manual_pdf} cat 4 output ${dist_dir}/${vh_egy_manual_path}_performance_specifications.pdf
  pdftk ${vh_egy_manual_pdf} cat 5 output ${dist_dir}/${vh_egy_manual_path}_table_of_contents.pdf
  pdftk ${vh_egy_manual_pdf} cat 6-13 output ${dist_dir}/${vh_egy_manual_path}_section1_general.pdf
  pdftk ${vh_egy_manual_pdf} cat 14-22 output ${dist_dir}/${vh_egy_manual_path}_section2_limitations.pdf
  pdftk ${vh_egy_manual_pdf} cat 23-36 output ${dist_dir}/${vh_egy_manual_path}_section3_emergency_procedures.pdf
  pdftk ${vh_egy_manual_pdf} cat 37-57 output ${dist_dir}/${vh_egy_manual_path}_section4_normal_procedures.pdf
  pdftk ${vh_egy_manual_pdf} cat 58 output ${dist_dir}/${vh_egy_manual_path}_stc_SA1000NW.pdf
  pdftk ${vh_egy_manual_pdf} cat 59-78 output ${dist_dir}/${vh_egy_manual_path}_section5_performance.pdf
  pdftk ${vh_egy_manual_pdf} cat 79-99 output ${dist_dir}/${vh_egy_manual_path}_section6_weight_and_balance.pdf
  pdftk ${vh_egy_manual_pdf} cat 100-132 output ${dist_dir}/${vh_egy_manual_path}_section7_airplane_and_systems_descriptions.pdf
  pdftk ${vh_egy_manual_pdf} cat 133-145 output ${dist_dir}/${vh_egy_manual_path}_section8_airplane_handling_and_maintenance.pdf
  pdftk ${vh_egy_manual_pdf} cat 146-202 output ${dist_dir}/${vh_egy_manual_path}_section9_supplements.pdf
fi

# Split Cessna 172R Factory Manual
cessna172r_manual_path="skyhawk_pim-20130523"
cessna172r_manual="${dist_dir}/${cessna172r_manual_path}"
cessna172r_manual_pdf="${cessna172r_manual}.pdf"
if [ -f "${cessna172r_manual_pdf}" ]; then
  pdftk ${cessna172r_manual_pdf} cat 1 output ${dist_dir}/${cessna172r_manual_path}_notice.pdf
  pdftk ${cessna172r_manual_pdf} cat 2-3 output ${dist_dir}/${cessna172r_manual_path}_performance_specifications.pdf
  pdftk ${cessna172r_manual_pdf} cat 5 output ${dist_dir}/${cessna172r_manual_path}_cover.pdf
  pdftk ${cessna172r_manual_pdf} cat 7 output ${dist_dir}/${cessna172r_manual_path}_table_of_contents.pdf
  pdftk ${cessna172r_manual_pdf} cat 9-38 output ${dist_dir}/${cessna172r_manual_path}_section1_general.pdf
  pdftk ${cessna172r_manual_pdf} cat 39-66 output ${dist_dir}/${cessna172r_manual_path}_section2_operating_limitations.pdf
  pdftk ${cessna172r_manual_pdf} cat 67-105 output ${dist_dir}/${cessna172r_manual_path}_section3_emergency_procedures.pdf
  pdftk ${cessna172r_manual_pdf} cat 107-154 output ${dist_dir}/${cessna172r_manual_path}_section4_normal_procedures.pdf
  pdftk ${cessna172r_manual_pdf} cat 155-176 output ${dist_dir}/${cessna172r_manual_path}_section5_performance.pdf
  pdftk ${cessna172r_manual_pdf} cat 177-200 output ${dist_dir}/${cessna172r_manual_path}_section6_weight_and_balance_equipment.pdf
  pdftk ${cessna172r_manual_pdf} cat 201-280 output ${dist_dir}/${cessna172r_manual_path}_section7_airplane_and_systems_descriptions.pdf
  pdftk ${cessna172r_manual_pdf} cat 281-305 output ${dist_dir}/${cessna172r_manual_path}_section8_airplane_handling_service_and_maintenance.pdf
  pdftk ${cessna172r_manual_pdf} cat 307-349 output ${dist_dir}/${cessna172r_manual_path}_section9_supplements.pdf
fi

# Split Cessna 172S Factory Manual
cessna172s_manual_path="skyhawksp_pim-20130523"
cessna172s_manual="${dist_dir}/${cessna172s_manual_path}"
cessna172s_manual_pdf="${cessna172s_manual}.pdf"
if [ -f "${cessna172s_manual_pdf}" ]; then
  pdftk ${cessna172s_manual_pdf} cat 1 output ${dist_dir}/${cessna172s_manual_path}_notice.pdf
  pdftk ${cessna172s_manual_pdf} cat 2-3 output ${dist_dir}/${cessna172s_manual_path}_performance_specifications.pdf
  pdftk ${cessna172s_manual_pdf} cat 5 output ${dist_dir}/${cessna172s_manual_path}_cover.pdf
  pdftk ${cessna172s_manual_pdf} cat 7 output ${dist_dir}/${cessna172s_manual_path}_table_of_contents.pdf
  pdftk ${cessna172s_manual_pdf} cat 9-38 output ${dist_dir}/${cessna172s_manual_path}_section1_general.pdf
  pdftk ${cessna172s_manual_pdf} cat 39-66 output ${dist_dir}/${cessna172s_manual_path}_section2_operating_limitations.pdf
  pdftk ${cessna172s_manual_pdf} cat 67-105 output ${dist_dir}/${cessna172s_manual_path}_section3_emergency_procedures.pdf
  pdftk ${cessna172s_manual_pdf} cat 107-154 output ${dist_dir}/${cessna172s_manual_path}_section4_normal_procedures.pdf
  pdftk ${cessna172s_manual_pdf} cat 155-178 output ${dist_dir}/${cessna172s_manual_path}_section5_performance.pdf
  pdftk ${cessna172s_manual_pdf} cat 179-202 output ${dist_dir}/${cessna172s_manual_path}_section6_weight_and_balance_equipment.pdf
  pdftk ${cessna172s_manual_pdf} cat 203-282 output ${dist_dir}/${cessna172s_manual_path}_section7_airplane_and_systems_descriptions.pdf
  pdftk ${cessna172s_manual_pdf} cat 283-307 output ${dist_dir}/${cessna172s_manual_path}_section8_airplane_handling_service_and_maintenance.pdf
  pdftk ${cessna172s_manual_pdf} cat 308-351 output ${dist_dir}/${cessna172s_manual_path}_section9_supplements.pdf
fi

# Split Cessna 182P Factory Manual
cessna182p_manual_path="cessna-182p"
cessna182p_manual="${dist_dir}/${cessna182p_manual_path}"
cessna182p_manual_pdf="${cessna182p_manual}.pdf"
if [ -f "${cessna182p_manual_pdf}" ]; then
  pdftk ${cessna182p_manual_pdf} cat 1 output ${dist_dir}/${cessna182p_manual_path}_cover.pdf
  pdftk ${cessna182p_manual_pdf} cat 2 output ${dist_dir}/${cessna182p_manual_path}_list_of_effective_pages.pdf
  pdftk ${cessna182p_manual_pdf} cat 3 output ${dist_dir}/${cessna182p_manual_path}_congratulations.pdf
  pdftk ${cessna182p_manual_pdf} cat 4 output ${dist_dir}/${cessna182p_manual_path}_performance_specifications.pdf
  pdftk ${cessna182p_manual_pdf} cat 5 output ${dist_dir}/${cessna182p_manual_path}_table_of_contents.pdf
  pdftk ${cessna182p_manual_pdf} cat 7-14 output ${dist_dir}/${cessna182p_manual_path}_section1_general.pdf
  pdftk ${cessna182p_manual_pdf} cat 15-25 output ${dist_dir}/${cessna182p_manual_path}_section2_limitations.pdf
  pdftk ${cessna182p_manual_pdf} cat 27-41 output ${dist_dir}/${cessna182p_manual_path}_section3_emergency_procedures.pdf
  pdftk ${cessna182p_manual_pdf} cat 43-64 output ${dist_dir}/${cessna182p_manual_path}_section4_normal_procedures.pdf
  pdftk ${cessna182p_manual_pdf} cat 65-91 output ${dist_dir}/${cessna182p_manual_path}_section5_performance.pdf
  pdftk ${cessna182p_manual_pdf} cat 93-117 output ${dist_dir}/${cessna182p_manual_path}_section6_weight_and_balance_equipment.pdf
  pdftk ${cessna182p_manual_pdf} cat 119-158 output ${dist_dir}/${cessna182p_manual_path}_section7_airplane_and_systems_descriptions.pdf
  pdftk ${cessna182p_manual_pdf} cat 159-172 output ${dist_dir}/${cessna182p_manual_path}_section8_airplane_handling_service_and_maintenance.pdf
  pdftk ${cessna182p_manual_pdf} cat 173-258 output ${dist_dir}/${cessna182p_manual_path}_section9_supplements.pdf
fi

# Split VH-TIN Manual
vh_tin_manual_path="vh-tin/cessna-182p_vh-tin"
vh_tin_manual="${dist_dir}/${vh_tin_manual_path}"
vh_tin_manual_pdf="${vh_tin_manual}.pdf"
if [ -f "${vh_tin_manual_pdf}" ]; then
  pdftk ${vh_tin_manual_pdf} cat 1 output ${dist_dir}/${vh_tin_manual_path}_amendment_record_sheet.pdf
  pdftk ${vh_tin_manual_pdf} cat 2-13 output ${dist_dir}/${vh_tin_manual_path}_certificate_of_registration.pdf
  pdftk ${vh_tin_manual_pdf} cat 14-15 output ${dist_dir}/${vh_tin_manual_path}_cover.pdf
  pdftk ${vh_tin_manual_pdf} cat 16-26 output ${dist_dir}/${vh_tin_manual_path}_amendment_record_sheets.pdf
  pdftk ${vh_tin_manual_pdf} cat 27-27 output ${dist_dir}/${vh_tin_manual_path}_introduction.pdf
  pdftk ${vh_tin_manual_pdf} cat 28-30 output ${dist_dir}/${vh_tin_manual_path}_list_of_contents.pdf
  pdftk ${vh_tin_manual_pdf} cat 31-31 output ${dist_dir}/${vh_tin_manual_path}_definitions.pdf
  pdftk ${vh_tin_manual_pdf} cat 32-34 output ${dist_dir}/${vh_tin_manual_path}_section1_general_aeroplane_particulars.pdf
  pdftk ${vh_tin_manual_pdf} cat 35-38 output ${dist_dir}/${vh_tin_manual_path}_section2_operating_limitations.pdf
  pdftk ${vh_tin_manual_pdf} cat 39-41 output ${dist_dir}/${vh_tin_manual_path}_section3_handling.pdf
  pdftk ${vh_tin_manual_pdf} cat 42-44 output ${dist_dir}/${vh_tin_manual_path}_section4_performance.pdf
  pdftk ${vh_tin_manual_pdf} cat 45-48 output ${dist_dir}/${vh_tin_manual_path}_section5_instrument_and_equipment_installations.pdf
  pdftk ${vh_tin_manual_pdf} cat 49-57 output ${dist_dir}/${vh_tin_manual_path}_section6_loading_data.pdf
  pdftk ${vh_tin_manual_pdf} cat 58-92 output ${dist_dir}/${vh_tin_manual_path}_section7_supplements.pdf
fi

# Split Rotax 912 Manual
rotax_912_manual_path="OM_912_Series_ED4_R0"
rotax_912_manual="${dist_dir}/${rotax_912_manual_path}"
rotax_912_manual_pdf="${rotax_912_manual}.pdf"
if [ -f "${rotax_912_manual_pdf}" ]; then
  pdftk ${rotax_912_manual_pdf} cat 1 output ${dist_dir}/${rotax_912_manual_path}_cover.pdf
  pdftk ${rotax_912_manual_pdf} cat 2 output ${dist_dir}/${rotax_912_manual_path}_rotax.pdf
  pdftk ${rotax_912_manual_pdf} cat 3 output ${dist_dir}/${rotax_912_manual_path}_table_of_contents.pdf
  pdftk ${rotax_912_manual_pdf} cat 5 output ${dist_dir}/${rotax_912_manual_path}_introduction.pdf
  pdftk ${rotax_912_manual_pdf} cat 7-8 output ${dist_dir}/${rotax_912_manual_path}_list_of_effective_pages.pdf
  pdftk ${rotax_912_manual_pdf} cat 9-10 output ${dist_dir}/${rotax_912_manual_path}_table_of_amendments.pdf
  pdftk ${rotax_912_manual_pdf} cat 11-29 output ${dist_dir}/${rotax_912_manual_path}_general_note.pdf
  pdftk ${rotax_912_manual_pdf} cat 31-41 output ${dist_dir}/${rotax_912_manual_path}_operating_instructions.pdf
  pdftk ${rotax_912_manual_pdf} cat 43-53 output ${dist_dir}/${rotax_912_manual_path}_standard_operation.pdf
  pdftk ${rotax_912_manual_pdf} cat 55-62 output ${dist_dir}/${rotax_912_manual_path}_abnormal_operation.pdf
  pdftk ${rotax_912_manual_pdf} cat 63-69 output ${dist_dir}/${rotax_912_manual_path}_performance_and_fuel_consumption.pdf
  pdftk ${rotax_912_manual_pdf} cat 71 output ${dist_dir}/${rotax_912_manual_path}_weights.pdf
  pdftk ${rotax_912_manual_pdf} cat 73-80 output ${dist_dir}/${rotax_912_manual_path}_system_description.pdf
  pdftk ${rotax_912_manual_pdf} cat 81-83 output ${dist_dir}/${rotax_912_manual_path}_preservation_and_storage.pdf
  pdftk ${rotax_912_manual_pdf} cat 85-86 output ${dist_dir}/${rotax_912_manual_path}_supplement.pdf
  pdftk ${rotax_912_manual_pdf} cat 87-88 output ${dist_dir}/${rotax_912_manual_path}_index.pdf
  pdftk ${rotax_912_manual_pdf} cat 89 output ${dist_dir}/${rotax_912_manual_path}_list_of_figures.pdf
  pdftk ${rotax_912_manual_pdf} cat 91 output ${dist_dir}/${rotax_912_manual_path}_back_cover.pdf
fi

# Split Dynon SV-D700/SV-D1000/SV-D1000T
dynon_sv_d700_sv_d1000_sv_d1000t_manual_path="sv-d700_sv-d1000_sv-d1000t/SkyView_System_Installation_Guide-Rev_W_v14_0"
dynon_sv_d700_sv_d1000_sv_d1000t_manual="${dist_dir}/${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}"
dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf="${dynon_sv_d700_sv_d1000_sv_d1000t_manual}.pdf"
if [ -f "${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf}" ]; then
  pdftk ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} cat 1 output ${dist_dir}/${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_cover.pdf # Cover
  pdftk ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} cat 3-4 output ${dist_dir}/${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_changes_and_contacts.pdf # Contact & Changes
  pdftk ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} cat 5-11 output ${dist_dir}/${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_table_of_contents.pdf # Table of Contents
  pdftk ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} cat 13-18 output ${dist_dir}/${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_introduction.pdf # 1 Introduction
  pdftk ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} cat 19-34 output ${dist_dir}/${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_system_planning.pdf # 2 System Planning
  pdftk ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} cat 35-53 output ${dist_dir}/${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_basic_skyview_display_operation.pdf # 3 Basic SkyView Display Operation
  pdftk ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} cat 55-87 output ${dist_dir}/${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_sv-d700_sv-d1000_sv-d1000t_installation_and_onfiguration.pdf # 4 SV-D700 / SV-D1000 / SV-D1000T Installation and Configuration
  pdftk ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} cat 89-106 output ${dist_dir}/${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_sv-adahrs-200_201_installation_and_configuration.pdf # 5 SV-ADAHRS-200/201 Installation and Configuration
  pdftk ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} cat 107-114 output ${dist_dir}/${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_sv-map-270_navigation_mapping_license_purchase_and_setup.pdf # 6 SV-MAP-270 Navigation Mapping License Purchase and Setup
  pdftk ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} cat 115-191 output ${dist_dir}/${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_sv-ems-220_221_installation_and_configuration.pdf # 7 SV-EMS-220/221 Installation and Configuration
  pdftk ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} cat 193-205 output ${dist_dir}/${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_sv-gps-250_sv-gps-2020_gps_receiver_installation_and_configuration.pdf # 8 SV-GPS-250 / SV-GPS-2020 GPS Receiver Installation and Configuration
  pdftk ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} cat 207-213 output ${dist_dir}/${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_sv-bat-320_installation.pdf # 9 SV-BAT-320 Installation
  pdftk ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} cat 215-236 output ${dist_dir}/${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_autopilot_servo_installation_configuration_and_calibration.pdf # 10 Autopilot Servo Installation, Configuration, and Calibration
  pdftk ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} cat 237-275 output ${dist_dir}/${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_sv-xpndr-261_262_installation_configuration_and_testing.pdf # 11 SV-XPNDR-261 / 262 Installation, Configuration, and Testing
  pdftk ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} cat 277-288 output ${dist_dir}/${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_sv-arinc-429_installation_and_configuration.pdf # 12 SV-ARINC-429 Installation and Configuration
  pdftk ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} cat 289-293 output ${dist_dir}/${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_vertical_power_vp-x_ntegration_and_onfiguration.pdf # 13 Vertical Power VP-X Integration and Configuration
  pdftk ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} cat 295-304 output ${dist_dir}/${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_sv-adsb-470_installation_configuration_and_testing.pdf # 14 SV-ADSB-470 Installation, Configuration, and Testing
  pdftk ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} cat 305-319 output ${dist_dir}/${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_accessory_installation_and_configuration.pdf # 15 Accessory Installation and Configuration
  pdftk ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} cat 321-339 output ${dist_dir}/${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_sv-com-c25_installation_configuration_and_testing.pdf # 16 SV-COM-C25 Installation, Configuration, and Testing
  pdftk ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} cat 341-362 output ${dist_dir}/${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_sv-com-x83_installation_configuration_and_testing.pdf # 17 SV-COM-X83 Installation, Configuration, and Testing
  pdftk ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} cat 363-371 output ${dist_dir}/${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_sv-ap-panel_installation.pdf # 18 SV-AP-PANEL Installation
  pdftk ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} cat 373-375 output ${dist_dir}/${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_sv-knob-panel_installation.pdf # 19 SV-KNOB-PANEL Installation
  pdftk ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} cat 377-385 output ${dist_dir}/${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_sv-mag-236_installation_and_configuration.pdf # 20 SV-MAG-236 Installation and Configuration
  pdftk ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} cat 387-399 output ${dist_dir}/${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_appendix_a_maintenance_and_troubleshooting.pdf # 21 Appendix A: Maintenance and Troubleshooting
  pdftk ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} cat 401-413 output ${dist_dir}/${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_appendix_b_specifications.pdf # 22 Appendix B: Specifications
  pdftk ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} cat 415-438 output ${dist_dir}/${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_appendix_c_wiring_and_electrical_connections.pdf # 23 Appendix C: Wiring and Electrical Connections
  pdftk ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} cat 439-440 output ${dist_dir}/${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_appendix_d_sv-ems-220_221_sensor_input_mapping_worksheet.pdf # 24 Appendix D: SV-EMS-220/221 Sensor Input Mapping Worksheet
  pdftk ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} cat 441-453 output ${dist_dir}/${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_appendix_e_serial_data_output.pdf # 25 Appendix E: Serial Data Output
  pdftk ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} cat 455-458 output ${dist_dir}/${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_appendix_f_user_data_logs.pdf # 26 Appendix F: User Data Logs
  pdftk ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} cat 459 output ${dist_dir}/${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_appendix_g_kavlico_pressure_sensor_part_numbers.pdf # 27 Appendix G: Kavlico Pressure Sensor Part Numbers
  pdftk ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} cat 461-463 output ${dist_dir}/${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_appendix_h_sensor_debug_data.pdf # 28 Appendix H: Sensor Debug Data
  pdftk ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} cat 465-489 output ${dist_dir}/${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_appendix_i_skyview_system_installation_guide_revision_history.pdf # 29 Appendix I: SkyView System Installation Guide Revision History
  pdftk ${dynon_sv_d700_sv_d1000_sv_d1000t_manual_pdf} cat 491-496 output ${dist_dir}/${dynon_sv_d700_sv_d1000_sv_d1000t_manual_path}_appendix_j_checklists.pdf # 30 Appendix J: Checklists
fi

# Split Dynon FlightDEK D-180
dynon_flightdek_d180_manual_path="flightdek-d180/FlightDEK-D180_Pilot's_User_Guide_Rev_H"
dynon_flightdek_d180_manual="${dist_dir}/${dynon_flightdek_d180_manual_path}"
dynon_flightdek_d180_manual_pdf="${dynon_flightdek_d180_manual}.pdf"
if [ -f "${dynon_flightdek_d180_manual_pdf}" ]; then
  pdftk ${dynon_flightdek_d180_manual_pdf} cat 1-2 output ${dist_dir}/${dynon_flightdek_d180_manual_path}_cover.pdf # Cover
  pdftk ${dynon_flightdek_d180_manual_pdf} cat 2-3 output ${dist_dir}/${dynon_flightdek_d180_manual_path}_contact_information.pdf # Contact Information
  pdftk ${dynon_flightdek_d180_manual_pdf} cat 4-7 output ${dist_dir}/${dynon_flightdek_d180_manual_path}_table_of_contacts.pdf # Table of Contents
  pdftk ${dynon_flightdek_d180_manual_pdf} cat 8-10 output ${dist_dir}/${dynon_flightdek_d180_manual_path}_introduction.pdf # Section 1 Introduction
  pdftk ${dynon_flightdek_d180_manual_pdf} cat 11-15 output ${dist_dir}/${dynon_flightdek_d180_manual_path}_product_overview.pdf # Section 2 Product Overview
  pdftk ${dynon_flightdek_d180_manual_pdf} cat 16-23 output ${dist_dir}/${dynon_flightdek_d180_manual_path}_product_operation.pdf # Section 3 Product Operation
  pdftk ${dynon_flightdek_d180_manual_pdf} cat 24-37 output ${dist_dir}/${dynon_flightdek_d180_manual_path}_available_pages.pdf # Section 4 Available Pages
  pdftk ${dynon_flightdek_d180_manual_pdf} cat 38-52 output ${dist_dir}/${dynon_flightdek_d180_manual_path}_efis_operation.pdf # Section 5 EFIS Operation
  pdftk ${dynon_flightdek_d180_manual_pdf} cat 53-62 output ${dist_dir}/${dynon_flightdek_d180_manual_path}_hsi_operation.pdf # Section 6 HSI Operation
  pdftk ${dynon_flightdek_d180_manual_pdf} cat 63-79 output ${dist_dir}/${dynon_flightdek_d180_manual_path}_autopilot_operation.pdf # Section 7 Autopilot Operation
  pdftk ${dynon_flightdek_d180_manual_pdf} cat 80-85 output ${dist_dir}/${dynon_flightdek_d180_manual_path}_alerts.pdf # Section 8 Alerts
  pdftk ${dynon_flightdek_d180_manual_pdf} cat 86-89 output ${dist_dir}/${dynon_flightdek_d180_manual_path}_ems_monitoring_functions.pdf # Section 9 EMS Monitoring Functions
  pdftk ${dynon_flightdek_d180_manual_pdf} cat 90-96 output ${dist_dir}/${dynon_flightdek_d180_manual_path}_ems_operation.pdf # Section 10 EMS Operation
  pdftk ${dynon_flightdek_d180_manual_pdf} cat 97-108 output ${dist_dir}/${dynon_flightdek_d180_manual_path}_appendix.pdf # Appendix
fi

# Split Dynon EFIS D-100
dynon_efis_d100_manual_path="efis-d100/EFIS-D100_Pilot's_User_Guide_Rev_H"
dynon_efis_d100_manual="${dist_dir}/${dynon_efis_d100_manual_path}"
dynon_efis_d100_manual_pdf="${dynon_efis_d100_manual}.pdf"
if [ -f "${dynon_efis_d100_manual_pdf}" ]; then
  pdftk ${dynon_efis_d100_manual_pdf} cat 1 output ${dist_dir}/${dynon_efis_d100_manual_path}_cover.pdf # Cover
  pdftk ${dynon_efis_d100_manual_pdf} cat 2-3 output ${dist_dir}/${dynon_efis_d100_manual_path}_contact_information.pdf # Contact Information
  pdftk ${dynon_efis_d100_manual_pdf} cat 4-6 output ${dist_dir}/${dynon_efis_d100_manual_path}_table_of_contents.pdf # Table of Contents
  pdftk ${dynon_efis_d100_manual_pdf} cat 7-9 output ${dist_dir}/${dynon_efis_d100_manual_path}_introduction.pdf # Section 1 Introduction
  pdftk ${dynon_efis_d100_manual_pdf} cat 10-14 output ${dist_dir}/${dynon_efis_d100_manual_path}_product_overview.pdf # Section 2 Product Overview
  pdftk ${dynon_efis_d100_manual_pdf} cat 15-22 output ${dist_dir}/${dynon_efis_d100_manual_path}_product_operation.pdf # Section 3 Product Operation
  pdftk ${dynon_efis_d100_manual_pdf} cat 23-33 output ${dist_dir}/${dynon_efis_d100_manual_path}_available_pages.pdf # Section 4 Available Pages
  pdftk ${dynon_efis_d100_manual_pdf} cat 34-49 output ${dist_dir}/${dynon_efis_d100_manual_path}_efis_operation.pdf # Section 5 EFIS Operation
  pdftk ${dynon_efis_d100_manual_pdf} cat 50-59 output ${dist_dir}/${dynon_efis_d100_manual_path}_hsi_operation.pdf # Section 6 HSI Operation
  pdftk ${dynon_efis_d100_manual_pdf} cat 60-76 output ${dist_dir}/${dynon_efis_d100_manual_path}_autopilot_operation.pdf # Section 7 Autopilot Operation
  pdftk ${dynon_efis_d100_manual_pdf} cat 77-80 output ${dist_dir}/${dynon_efis_d100_manual_path}_alerts.pdf # Section 8 Alerts
  pdftk ${dynon_efis_d100_manual_pdf} cat 81-89 output ${dist_dir}/${dynon_efis_d100_manual_path}_appendix.pdf # Appendix
fi

# Split Garmin G500/G600
garmin_g500_g600_manual_path="d500_d600/190-00601-02_K"
garmin_g500_g600_manual="${dist_dir}/${garmin_g500_g600_manual_path}"
garmin_g500_g600_manual_pdf="${garmin_g500_g600_manual}.pdf"
if [ -f "${garmin_g500_g600_manual_pdf}" ]; then
  pdftk ${garmin_g500_g600_manual_pdf} cat 1 output ${dist_dir}/${garmin_g500_g600_manual_path}_cover.pdf # Cover
  pdftk ${garmin_g500_g600_manual_pdf} cat 2-10 output ${dist_dir}/${garmin_g500_g600_manual_path}_foreword.pdf # Foreword
  pdftk ${garmin_g500_g600_manual_pdf} cat 11-16 output ${dist_dir}/${garmin_g500_g600_manual_path}_table_of_contents.pdf # Table of Contents
  pdftk ${garmin_g500_g600_manual_pdf} cat 17-42 output ${dist_dir}/${garmin_g500_g600_manual_path}_system_overview.pdf # Section 1 System Overview
  pdftk ${garmin_g500_g600_manual_pdf} cat 43-82 output ${dist_dir}/${garmin_g500_g600_manual_path}_primary_flight_display.pdf # Section 2 Primary Flight Display
  pdftk ${garmin_g500_g600_manual_pdf} cat 83-158 output ${dist_dir}/${garmin_g500_g600_manual_path}_multi_function_display.pdf # Section 3 Multi-Function Display
  pdftk ${garmin_g500_g600_manual_pdf} cat 159-316 output ${dist_dir}/${garmin_g500_g600_manual_path}_hazard_avoidance.pdf # Section 4 Hazard Avoidance
  pdftk ${garmin_g500_g600_manual_pdf} cat 317-368 output ${dist_dir}/${garmin_g500_g600_manual_path}_additional_features.pdf # Section 5 Additional Features
  pdftk ${garmin_g500_g600_manual_pdf} cat 369-384 output ${dist_dir}/${garmin_g500_g600_manual_path}_annunciations_and_alerts.pdf # Section 6 Annunciations and Alerts
  pdftk ${garmin_g500_g600_manual_pdf} cat 385-392 output ${dist_dir}/${garmin_g500_g600_manual_path}_symbols.pdf # Section 7 Symbols
  pdftk ${garmin_g500_g600_manual_pdf} cat 393-400 output ${dist_dir}/${garmin_g500_g600_manual_path}_glossary.pdf # Section 8 Glossary
  pdftk ${garmin_g500_g600_manual_pdf} cat 401-408 output ${dist_dir}/${garmin_g500_g600_manual_path}_appendix_a.pdf # Appendix A
  pdftk ${garmin_g500_g600_manual_pdf} cat 409-418 output ${dist_dir}/${garmin_g500_g600_manual_path}_index.pdf # Index
fi

# Split AvMap EKP IV/Pro
avmap_ekp_iv_manual_path="ekpiv_ekpiv_pro/EKP_pro_inglese_162"
avmap_ekp_iv_manual="${dist_dir}/${avmap_ekp_iv_manual_path}"
avmap_ekp_iv_manual_pdf="${avmap_ekp_iv_manual}.pdf"
if [ -f "${avmap_ekp_iv_manual_pdf}" ]; then
  pdftk ${avmap_ekp_iv_manual_pdf} cat 1 output ${dist_dir}/${avmap_ekp_iv_manual_path}_cover.pdf # Cover
  pdftk ${avmap_ekp_iv_manual_pdf} cat 2-3 output ${dist_dir}/${avmap_ekp_iv_manual_path}_foreword.pdf # Foreword
  pdftk ${avmap_ekp_iv_manual_pdf} cat 4-8 output ${dist_dir}/${avmap_ekp_iv_manual_path}_table_of_contents.pdf # Table of Contents
  pdftk ${avmap_ekp_iv_manual_pdf} cat 5-15 output ${dist_dir}/${avmap_ekp_iv_manual_path}_introduction.pdf # Section 1 Introduction
  pdftk ${avmap_ekp_iv_manual_pdf} cat 16-21 output ${dist_dir}/${avmap_ekp_iv_manual_path}_the_basics.pdf # Section 2 The Basics
  pdftk ${avmap_ekp_iv_manual_pdf} cat 22-35 output ${dist_dir}/${avmap_ekp_iv_manual_path}_the_moving_maps.pdf # Section 3 The Moving Maps
  pdftk ${avmap_ekp_iv_manual_pdf} cat 36-37 output ${dist_dir}/${avmap_ekp_iv_manual_path}_navigation_and_location.pdf # Section 4 Navigation and Location
  pdftk ${avmap_ekp_iv_manual_pdf} cat 38-39 output ${dist_dir}/${avmap_ekp_iv_manual_path}_the_hsi_screen.pdf # Section 5 The HSI Screen
  pdftk ${avmap_ekp_iv_manual_pdf} cat 40-44 output ${dist_dir}/${avmap_ekp_iv_manual_path}_flight_plan.pdf # Section 6 Flight Plan
  pdftk ${avmap_ekp_iv_manual_pdf} cat 45-47 output ${dist_dir}/${avmap_ekp_iv_manual_path}_the_global_positioning_system.pdf # Section 7 The Global Positioning System
  pdftk ${avmap_ekp_iv_manual_pdf} cat 48-51 output ${dist_dir}/${avmap_ekp_iv_manual_path}_waypoint_and_database.pdf # Section 8 Waypoint and Database
  pdftk ${avmap_ekp_iv_manual_pdf} cat 52-53 output ${dist_dir}/${avmap_ekp_iv_manual_path}_approach_data_procedures.pdf # Section 9 Approach Data Procedures
  pdftk ${avmap_ekp_iv_manual_pdf} cat 54-59 output ${dist_dir}/${avmap_ekp_iv_manual_path}_calculator.pdf # Section 10 Calculator
  pdftk ${avmap_ekp_iv_manual_pdf} cat 60-61 output ${dist_dir}/${avmap_ekp_iv_manual_path}_the_checklists.pdf # Section 11 The Checklists
  pdftk ${avmap_ekp_iv_manual_pdf} cat 62-63 output ${dist_dir}/${avmap_ekp_iv_manual_path}_simulator.pdf # Section 12 Simulator
  pdftk ${avmap_ekp_iv_manual_pdf} cat 64-65 output ${dist_dir}/${avmap_ekp_iv_manual_path}_the_communication.pdf # Section 13 The Communication
  pdftk ${avmap_ekp_iv_manual_pdf} cat 66-75 output ${dist_dir}/${avmap_ekp_iv_manual_path}_the_system_setup_menu.pdf # Section 14 The System Setup Menu
  pdftk ${avmap_ekp_iv_manual_pdf} cat 76-82 output ${dist_dir}/${avmap_ekp_iv_manual_path}_operating_requirements.pdf # Section 15 Operating Requirements
  pdftk ${avmap_ekp_iv_manual_pdf} cat 83 output ${dist_dir}/${avmap_ekp_iv_manual_path}_ekp_iv_pro.pdf # Section 16 EKP IV Pro
  pdftk ${avmap_ekp_iv_manual_pdf} cat 84-95 output ${dist_dir}/${avmap_ekp_iv_manual_path}_appendices.pdf # Appendices
fi

pdf_files=$(cd ${dist_dir} > /dev/null && find . -type f -name "*.pdf")

OIFS=$IFS
IFS=$'\n'
for pdf_file in $pdf_files
do
  basename=$(basename -- "${pdf_file}")
  dirname=$(dirname -- "${pdf_file}")
  filename="${basename%.*}"

  page1_pdf_dir=${dist_dir}/${dirname}
  page1_pdf="${page1_pdf_dir}/${filename}_page1.pdf"

  mkdir -p "${page1_pdf_dir}"

  gs -q -sDEVICE=pdfwrite -dNOPAUSE -dBATCH -dSAFER -dFirstPage=1 -dLastPage=1 -sOutputFile=${page1_pdf} ${dist_dir}/${pdf_file}

  page1_pdf_png="${page1_pdf}.png"

  convert ${page1_pdf} ${page1_pdf_png}

  for size in 600 450 350 250 150 100 65 45
  do
    page1_pdf_png_size="${page1_pdf}-${size}.png"
    convert ${page1_pdf_png} -resize ${size}x${size} ${page1_pdf_png_size}
  done
done
IFS="$OIFS"
